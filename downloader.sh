#!/bin/bash

# disable debug log by default
dbg_lvl=0
function log {
  log_lvl=$1
  if [ $dbg_lvl -gt $log_lvl ]; then
    echo -e "$2"
  fi
}

# check if config is in place copy example if that's not the case
if [ -f config.sh ]; then
  source config.sh
else 
  cp config.sh.example config.sh
  source config.sh
fi 

mkdir -p $BASE_DIR
cd $BASE_DIR

function npo_search { 
  title=$1
  
  term=$(echo $title | sed 's/\ /\-/g'); 
  result=$(curl -s "https://www.npo.nl/search?query=$term" -H 'x-requested-with: XMLHttpRequest' | grep "npo-tile-link" | head -n 1 |sed 's/.*title/title/g; s/\ href/\nhref/g; s/\ class=".*//g'  )
  
  result_title=$(echo -e "$result" | grep "title=" | sed 's/title=//g; s/"//g')
  result_url=$(echo -e "$result" | grep "href=" | sed 's/href=//g; s/"//g')
  result_tag=$(echo -e "$result_url" | sed 's/https:\/\/www.npo.nl\///g; s/"//g' | awk -F '/' '{print $1}' )
  result_id=$(echo -e "$result_url" | sed 's/https:\/\/www.npo.nl\///g; s/"//g' | awk -F '/' '{print $2}' )
  
  log 1 "Title: $result_title"
  log 1 "URL:   $result_url"
  log 1 "TAG:   $result_tag"
  log 1 "ID:    $result_id"
}

function npo_download {
  showID=$(echo $1 | sed 's/.*/\L&/')
  url_list=$(curl -s https://npo.nl/sitemap/franchise_$showID.xml | sed 's/>/\n/g' | grep "</video:player_loc" | sed 's/<\/video:player_loc//g')
  log 2 "$url_list"
  for url in $url_list ; do
    youtube-dl $url
  done
}

series=$(echo "$series" | sed 's/\ /\-/g'); 
for serie in $series; do
  log 1 "Serie: $serie"
  npo_search $serie

  mkdir $BASE_DIR/$result_tag
  cd $BASE_DIR/$result_tag
  npo_download $result_id

done

ls -lahtr $BASE_DIR/*
